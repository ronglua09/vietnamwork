 $(document).ready(function(){
    var options = {
        nextButton: true,
        prevButton: true,
        pagination: true,
        animateStartingFrameIn: true,
        autoPlay: true,
        autoPlayDelay: 3000,
        preloader: true,
        preloadTheseFrames: [1],
        preloadTheseImages: [
            "content/DienThoai.png",
            "content/timthumb (1).png",
            "content/web07.jpg"
        ]
    };
    
    var mySequence = $("#sequence").sequence(options).data("sequence");
});
$(function() {
    var $galitem = $('.thumbnail').children();
    // Đếm các ảnh trong gallery
    var $galsize = $('.thumbnail').size();
	var $consize = $('.thumbnail .caption').size();
    // Thêm nút Prev và Next vào gallery
	$('.col-md-3').append('<div id="galprev">Prev</div><div id="galnext">Next</div>');
    // Ẩn tất cả các ảnh và hiện ảnh đầu tiên
	    $('.thumbnail img:gt(0)').hide();
		$('.thumbnail .caption:gt(0)').hide();

    $currentimg = 0;

    // Thêm id để phân biệt riêng từng ảnh

    $galitem.attr("id", function (arr) {

        return "galleryitem" + arr;

    });


    // Thêm sự kiện click vào nút Prev

    $('#galprev').click(function () {

        if ($currentimg > 0) {

            previmg($currentimg);


        }

    });

    // Thêm sự kiện click vào nút Next

    $('#galnext').click(function () {
        if ($currentimg < $galsize - 1) {

            nextimg($currentimg, $galsize);

            $currentimg = $currentimg + 1;

        }
    });
})
// Hàm xử lý khi nút Next được ấn
function nextimg($img, $size) {
    $n_img = $img + 1;
    if ($n_img < $size) {
        $('#galleryitem' + $img).fadeOut();
        $('#galleryitem' + $n_img).fadeIn();

    }
}
// Hàm xử lý khi nút Previous được ấn
function previmg($img) {
    $p_img = $img - 1;
    if ($p_img >= 0) {
        $('#galleryitem' + $img).fadeOut();
        $('#galleryitem' + $p_img).fadeIn();
    }
}
